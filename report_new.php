<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}
$recordsForDate = isset($_POST['date']) ? substr($_POST['date'],0,4)."-".substr($_POST['date'],5,2)."-".substr($_POST['date'],8,2) : date("Y-m-d");
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
	<script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#date').datepicker({ dateFormat: 'yy-mm-dd' });
        //.datepicker("setDate", new Date())
    })
  </script>
</head>

<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
	<form name="frm" action="report_new.php" method="post">
		Select Retailer : <select name="user_id" onchange="frm.submit();">
			<option value="">All</option>
			<?php
			$sSQL = "SELECT user_id,username FROM users WHERE usertype = 1 ORDER BY username";
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0){
				while($row = mysql_fetch_array($rs)){
					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';
					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';
				}
			}
			?>
		</select>
		<input type="text" id="date" name="date" placeholder="Select Date" 
           value="<?php  echo $recordsForDate;  ?>">
    <input type="submit" name="submitBtn" value="Display !">
	</form>	
	
	<div id="mainWrapper" style="margin-top:20px;">
		<div class="box-body table-responsive">
				<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
					<thead>
						<tr>
						   <th rowspan="2">Draw Id</th>
							<th rowspan="2">Draw DateTime</th>
							<th colspan="11">Product</th>                                               
							<th colspan="3">&nbsp;</th>
						</tr>
						<tr>                                                
							<th>1</th><th>2</th>
							<th>3</th><th>4</th>
							<th>5</th><th>6</th>
							<th>7</th><th>8</th>
							<th>9</th><th>10</th>
							<th>Total</th>
							<th>Win</th>
							<th>&nbsp;</th>
							<th>Percent</th>
						</tr>
					</thead>
					<tbody>                  
					<?php
					$products = array();
					$totalArray = array();
					for($i=0;$i<10;$i++)
					{
						$products[$i] = $i+1; 
						$totalArray[$i] = 0;
					}
					$totalArray[10] = 0;
					$totalArray[11] = 0;
					$m=0;
					$qry = "SELECT * FROM draw
					        WHERE drawdatetime LIKE '".$recordsForDate."%'
					        ORDER BY drawdatetime";
					//$qry = "SELECT * FROM draw WHERE draw_id >= 3868 AND draw_id < 3870 ORDER BY drawdatetime";
					
					$res = mysql_query($qry) or print(mysql_error());											
					$nums = mysql_num_rows($res);
					
					while($row = mysql_fetch_array($res))
					{												
						$totalProducts = 0;
						if($nums != 0)
						{
							$drawid = $row['draw_id'];
							echo "<tr><td>".$drawid."</td>";
							$time = date("d-m-Y h:i:s",strtotime($row['drawdatetime']));										
							echo "<td>".$time."</td>";
							$qry2 = "SELECT * FROM receipt_master where draw_id='$drawid' AND receipt_cancel = 0 ";
							if(isset($_POST["user_id"]) && trim($_POST["user_id"]) != ""){
								$qry2 = $qry2 . " AND retailer_id = ".trim($_POST["user_id"]);
							}
							$res2 = mysql_query($qry2) or print(mysql_error());
							$num_rows = mysql_num_rows($res2);
							if($num_rows != 0)
							{
								$rec_details_id = array();
								while($row2 = mysql_fetch_array($res2))
								{	
									$receiptid = $row2['receipt_id'];
									$recid[] = $receiptid;
									
									$qry3 = "SELECT * FROM receipt_details where receipt_id='$receiptid'";												
									$res3 = mysql_query($qry3) or print(mysql_error());
									while($row3 = mysql_fetch_array($res3))
									{									
										$productid[] = $row3['product_id'];
										$quantity[] = $row3['quantity'];
										$rec_details_id[] = $row3['receipt_details_id'];														
									}												
								}											
						
								if(is_array($rec_details_id) && count($rec_details_id) > 0)
									$recdetails = implode(",",$rec_details_id);		
								else
									$recdetails = 0;					
								for($i=0;$i<10;$i++)
								{		
									$qry4 = "SELECT COUNT(product_id) as cnt,product_id,SUM(quantity) as quantity FROM receipt_details where product_id='$products[$i]' and receipt_details_id IN ($recdetails) ";
									$res4 = mysql_query($qry4) or print(mysql_error());
									$rows4 = mysql_fetch_array($res4);
									if($rows4['cnt'] != 0)											
									{														
										echo "<td align='right'>".$rows4['quantity']."</td>";	
										$totalProducts += $rows4['quantity'];
										$totalArray[$i] = $totalArray[$i] + $rows4['quantity'];
									}
									else
									{
										echo "<td align='right'>0</td>";
									}
								}
								$totalArray[10] = $totalArray[10] + $totalProducts;
							}
							else
							{
								for($i=0;$i<10;$i++)
								{	
									echo "<td align='right'>0</td>";
								}
							}									
							$totalArray[11] = $totalArray[11] + $row['win_product_id'];
							echo "<td align='right'>".$totalProducts."</td>";
							echo "<td align='right'>".$row['win_product_id']."</td>";
							echo "<td align='right'>".$row['is_jackpot']."</td>";
							echo "<td align='right'>".$row['percent']."</td>";
						
							unset($recdetails);
							unset($rec_details_id);
							unset($productid);
							unset($quantity);
							$m++;										
						}
						else
						{
							for($i=0;$i<10;$i++)
							{	
								echo "<td align='right'>0</td>";
							}
						}
					}
					/*									
					echo "<pre>";print_r($recdetails);
					echo "<pre>";print_r($productid);												
					print_r($quantity);
					*/				
					
					?>
					</tbody>
					<tfoot>
						<tr>                                                
							<th colspan="2" align="right">Total</th>
							<?php
							for($i=0;$i<=11;$i++)
							{	
								echo "<th align='right'>".$totalArray[$i]."</th>";
							}
							?>
							<th colspan="3"></th>
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
	
	</div> 
</body>
</html>

