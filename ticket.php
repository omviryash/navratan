<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['username'])){
	header('Location: ./index.php');
	exit;
}
if(isset($_REQUEST["id"])) $receipt_id = $_REQUEST["id"]; else $receipt_id = 0;
if(!isset($_REQUEST["lastrec"])){
	$sSQL = "SELECT company_id,draw_id,hash_key,receipt_time,retailer_id FROM receipt_master WHERE receipt_id = ".$receipt_id;
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);

}else{
	$sSQL = "SELECT company_id,draw_id,hash_key,receipt_time,retailer_id,receipt_id FROM receipt_master WHERE company_id = ".$company_id." AND retailer_id = ".$_SESSION['user_id']." AND receipt_time < '".date("Y-m-d H:i:s")."' ORDER BY receipt_id DESC LIMIT 0,1";
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);
	$receipt_id = $row["receipt_id"];
}

//get company details
$cid = $row['company_id'];
$qry = "SELECT company_name FROM company WHERE company_id = '$cid'";
$resu= mysql_query($qry) or print(mysql_error());
$rescomp = mysql_fetch_array($resu);

//get draw details
$drawid = $row['draw_id'];
$drqry = "SELECT drawdatetime FROM draw WHERE draw_id = '$drawid'";
$drres= mysql_query($drqry) or print(mysql_error());
$resdraw = mysql_fetch_array($drres);

//$row['company_id'];
$sSQL = "SELECT product_id,quantity,product_price FROM receipt_details WHERE receipt_id = ".$receipt_id;
$rs1 = mysql_query($sSQL) or print(mysql_error());

?>
<html>
<head>
	<style>
		@media print { .pagebreak { page-break-after:always; } }
		body { margin:0px; padding:0px; font-size:9px; font-weight:bold; text-transform: uppercase; font-family:Verdana, Geneva, sans-serif;}
	</style>
</head>
<body>
	<div class="mainwrapper newwrapper" style="margin-left:10px;">		
		<div class="subwrapper" style="border-bottom:dotted 2px #514F4F;"><b>Acknowledgement Receipt - <?php echo $receipt_id?></b></div>
		<?php if($row["hash_key"] != "" && file_exists("images/codes/".$row["hash_key"].".gif")){ ?>
		<div><img src="<?php echo "images/codes/".$row["hash_key"].".gif"; ?>" /></div>
		<?php } ?>
		<div class="subdate"><span><?php echo date("d/m/Y h:i:s A",strtotime($row['receipt_time']));?></span>&nbsp;&nbsp;&nbsp;<span>C.ID - <?php echo rtnretailer($row['retailer_id']);?></span></div>
		<div class="subdate"><b><?php echo $rescomp['company_name']?></b></div>
		<div class="subdate"><b>Retailer : <?php echo $_SESSION['user_id']?></b></div>
		<div class="subdate"><b><span>Date : <?php echo date("d/m/Y",strtotime($resdraw['drawdatetime']));?></span>&nbsp;&nbsp;&nbsp;<span>Time : <?php echo date("h:i A",strtotime($resdraw['drawdatetime']));?></span></b></div>
		<?php 
		$totqty = $totamt = 0;
		if(mysql_num_rows($rs1) > 0){		
			while($rows = mysql_fetch_array($rs1)){
		
					$qry = "SELECT product_name FROM product WHERE product_id = ".$rows["product_id"];
					$rs2= mysql_query($qry) or print(mysql_error());
					$row2 = mysql_fetch_array($rs2);
					
					echo '<div class="subdate">'.$row2["product_name"].' * '.$rows["quantity"].'</div>';
					//echo "<pre>";print_r($rows);
					$totqty += $rows["quantity"];
					$totamt = $totamt + ($rows["quantity"]*$rows["product_price"]);
			}
		}
		?>
		<div class="subdate" style="margin-top:10px;"><b><span>Product Qty : <?php echo $totqty;?></span>&nbsp;&nbsp;&nbsp;<span>Total Amt : <?php echo formatAmt($totamt);?></span></b></div>
		<div class="subdate">*Conditions Apply :</div>
		<div class="subdate">For Terms & Conditions P.T.O.</div>
		<div class="subdate">This Acknowlegement slip should be</div>
		<div class="subdate">retained to redeem the products.</div><br>
		<hr style="border:solid 1px #000;">
	</div>
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
		javascript:window.print();
		$(function () {
			val = 1;
			$( document ).on( 'keydown', function ( e ) {
				if ( e.keyCode === 13 ) { 
					if(val == 1)
						val = val + 1;
					else
					window.close();
				}
			});
			
			$(document).keydown();
			$(document).keydown();
		});
	</script>
	<div class="pagebreak"></div>
</body>
</html>