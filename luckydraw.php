<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
  <script type="text/javascript" src="js/jquery-ui.js"></script>  
</head>
<body>
		<div class="mainwrapper" style="margin:0px auto; padding:10px; text-align:center">
		 <form method="post" action="luckydraw.php" id="showtimeform">
			Date: <input type="text" id="frmdate" name="frmdate" placeholder="Select Date" value="<?php if (isset($_POST['frmdate'])) { echo $_POST['frmdate']; } ?>">
			<input type="button" name="showtime" value="Load Draw" id="printpurchase">
			<input type="button" name="back" value="Back" id="back">
		</div>
		<script language="javascript">
        $(document).ready(function() {
            $('#frmdate').datepicker({ dateFormat: 'dd-mm-yy' });
            
            $('#printpurchase').on('click', function() {
		            if ($('#frmdate').val() != '') {
		                $('#showtimeform').submit();
		            }
		            else {
		                alert("Please select date");
		                return false;
		            }
		        });
		        $('#back').on('click', function() {
		            window.close();
		        });
        })
        
    </script>
	<?php
		if(isset($_POST['frmdate'])){
		$frmdate = date('Y-m-d', strtotime($_POST['frmdate']));
		$sSQL = "SELECT win_product_id,DATE_FORMAT(d.drawdatetime,'%h:%i %p') as currentTime,is_jackpot FROM draw d,product p WHERE DATE_FORMAT(d.drawdatetime,'%Y-%m-%d') = '".$frmdate."' AND DATE_FORMAT(d.drawdatetime,'%Y-%m-%d') != '2014-11-04'";
		if($frmdate == date('Y-m-d')) $sSQL .= " AND DATE_FORMAT(drawdatetime,'%H:%i:%s') <= '".date("H:i:s")."'";
		$sSQL .= " AND p.product_id = d.win_product_id ORDER BY d.drawdatetime";
		$rs = mysql_query($sSQL);
			
		if(mysql_num_rows($rs) > 0){
		?>
		<div id="yantra_content" style="border:1px solid black;width:900px;margin:0px auto;padding:10px;background:none;border:0px;box-shadow:none">		
			<div style="font-size:20px; text-align:center; border-bottom:dotted 2px #514F4F; padding-bottom:5px; margin-bottom:5px;">Lucky Draw</div>
			<div style="text-align:center;margin-bottom:10px;"><b><span>From Date: <?php echo date("d/m/Y", strtotime($_POST['frmdate']));?></span></b></div>
			<div id="content" style="width:975px;height:auto;">
			<ul>
				<?php 
				while($row = mysql_fetch_array($rs)){
					if($row["win_product_id"] < 10) $winproductit = "0".$row["win_product_id"]; else $winproductit = $row["win_product_id"];
					if($row["is_jackpot"] == 'YES'){
					?>
						<li><div class="yantralist jp"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"><img src="./images/star.gif" style="width:30px !important" height="25px" /></div></div><div class="rightyantra jackport"><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /></div></div></li>
					<?php 
					}else{
					?>
						<li><div class="yantralist"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"></div></div><div class="rightyantra"><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /></div></div></li>
					<?php
					}
				} ?>
			</ul>
			</div>
		</div>
		
		<?php
		}else{
			echo "No records found.";
		}
	}
	?>
</body>
</html>

