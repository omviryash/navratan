<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<body>
	<?php
	if(isset($_POST['frmdate']) && isset($_POST['todate'])){
		?>
		<style>
			@media print { .pagebreak { page-break-after:always; } }
			body { margin:0px; padding:0px; font-size:12px; }
			table td {font-size:12px; }
		</style>
		<?php
		$frmdate = date('Y-m-d', strtotime($_POST['frmdate']));
		$todate = date('Y-m-d', strtotime($_POST['todate']));
		$sale = $saleQty = $purchase = $silver = $silverQty = $cancel = $cancelQty = 0;
		$sSQL = "SELECT receipt_id,user_commission,d.win_product_id,d.win_amount FROM receipt_master rm,draw d WHERE (DATE_FORMAT(drawdatetime,'%Y-%m-%d') BETWEEN '".$frmdate."' AND '".$todate."') AND rm.draw_id = d.draw_id AND receipt_cancel = 0 AND retailer_id = ".$_SESSION['user_id'];
		$rs = mysql_query($sSQL);
		if(mysql_num_rows($rs) > 0){
			while($row = mysql_fetch_array($rs)){
				$sSQL = "SELECT quantity,product_price,product_id FROM receipt_details WHERE receipt_id = '".$row["receipt_id"]."'";
				$rs1 = mysql_query($sSQL);
				while($row1 = mysql_fetch_array($rs1)){
					$tmpamt = ($row1["quantity"] * $row1["product_price"]);
					$sale = $sale + $tmpamt;
					$saleQty = $saleQty + $row1["quantity"];
					$purchase = ($purchase + $tmpamt) - ($tmpamt*$row["user_commission"])/100;
					if($row1["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0){
						$silverQty = $silverQty + $row1["quantity"];
						$silver = $silver + ($row1["quantity"] * $row["win_amount"]);
					}
				}
			}
		}
		
		$sSQL = "SELECT receipt_id,user_commission FROM receipt_master rm,draw d WHERE DATE_FORMAT(drawdatetime,'%Y-%m-%d') = '".date("Y-m-d")."' AND rm.draw_id = d.draw_id AND receipt_cancel = 1 AND retailer_id = ".$_SESSION['user_id'];
		$rs2 = mysql_query($sSQL);
		if(mysql_num_rows($rs2) > 0){
			while($row2 = mysql_fetch_array($rs2)){
				$sSQL = "SELECT quantity,product_price FROM receipt_details WHERE receipt_id = '".$row2["receipt_id"]."'";
				$rs1 = mysql_query($sSQL);
				while($row1 = mysql_fetch_array($rs1)){
					$tmpamt = ($row1["quantity"] * $row1["product_price"]);
					$cancel = $cancel + $tmpamt;
					$cancelQty = $cancelQty + $row1["quantity"];
				}
			}
		}	
		if($sale > 0){
		?>
		<div class="mainwrapper" style="margin-left:10px;">		
			<div class="subwrapper" style="border-bottom:dotted 2px #514F4F;"><b>Purchase Receipt</b></div>
			<div class="subdate"><b><span>From Date: <?php echo date("d/m/Y", strtotime($_POST['frmdate']));?><br />To Date: <?php echo date("d/m/Y", strtotime($_POST['todate']));?> </span></b></div>
			
			<table width="15%" cellspacing="0" cellpadding="1" id="tableList">
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;">&nbsp;</td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><b>Amount</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;"><b>Qty</b></td>
					</tr>
					<tr>
						<td><b>Sale</b></td>
						<td align="right"><?php echo formatAmt($sale); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $saleQty; ?></td>
					</tr>
					<tr>
						<td><b>Purchase</b></td>
						<td align="right"><?php echo formatAmt($purchase); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $saleQty; ?></td>
					</tr>
					<tr>
						<td><b>Silver Points<br />(Worth Rs.)</b></td>
						<td align="right"><?php echo formatAmt($silver); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $silverQty; ?></td>
					</tr>
					<tr>
						<td><b>Commission</b></td>
						<td align="right"><?php echo formatAmt($sale-$purchase); ?></td>
						<td align="right" style="padding-right:10px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Bal</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><?php echo formatAmt($purchase-$silver); ?></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;">&nbsp;</td>
					</tr>
					<!--<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Cancel</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><?php echo formatAmt($cancel); ?></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;"><?php echo $cancelQty; ?></td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Payable</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><?php echo formatAmt(($purchase-$silver)-$cancel); ?></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;">&nbsp;</td>
					</tr>-->
			</table>
		</div>
		<hr style="border:solid 1px #000;">
		<script type="text/javascript">
		javascript:window.print();
		$(function () {
			$( document ).on( 'keydown', function ( e ) {
				if ( e.keyCode === 13 ) { 
					window.setTimeout(function () {
					window.close();
					}, 1000); // 1 sec
				}
			});
		});
		</script>
		<div class="pagebreak"></div>
		<?php
		}else{
			echo "No records found.";
		}
	}else{
		?>
		<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
	  <script type="text/javascript" src="js/jquery-ui.js"></script>  
		<div class="formwrapper">
		 <form method="post" action="purchase.php" id="showtimeform">
			From Date: <input type="text" id="frmdate" name="frmdate" placeholder="Select Date" value="<?php if (isset($_POST['frmdate'])) { echo $_POST['frmdate']; } ?>">
			To Date: <input type="text" id="todate" name="todate" placeholder="Select Date" value="<?php if (isset($_POST['todate'])) { echo $_POST['todate']; } ?>">
			<input type="button" name="showtime" value="Print" id="printpurchase">
			<input type="button" name="back" value="Back" id="back">
		</div>
		<script language="javascript">
        jQuery(document).ready(function() {
            jQuery('#frmdate').datepicker({ dateFormat: 'dd-mm-yy' });
            jQuery('#todate').datepicker({ dateFormat: 'dd-mm-yy' });
            
            jQuery('#printpurchase').on('click', function() {
		            if (jQuery('#frmdate').val() != '' && jQuery('#todate').val() != '') {
		                jQuery('#showtimeform').submit();
		            }
		            else {
		                alert("Please select dates");
		                return false;
		            }
		        });
		        jQuery('#back').on('click', function() {
		            window.close();
		        });
        })
        
    </script>
		<?php
	}
	?>
</body>
</html>

